# Map4's CSV to TF

This package reads outputs of MapIV Engine (CSV) and publishes them as ROS TF messages. 

`roslaunch m4_csv2tf tf_publisher.launch map4_engine_csv:=MAP4ENGINE.csv localizer_frame_id:=base_link points_topic:=/pointcloud_topic`