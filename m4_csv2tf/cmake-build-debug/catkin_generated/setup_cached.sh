#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/ne0/m4_csv2tf/src/m4_csv2tf/cmake-build-debug/devel:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH='/opt/ros/melodic/lib:/usr/local/cuda/lib64:/home/ne0/Downloads/nvidia/TensorRT-7.0.0.11/lib'
export PATH='/opt/ros/melodic/bin:/home/ne0/Downloads/nvidia/TensorRT-7.0.0.11/bin:/home/ne0/anaconda3/condabin:/usr/local/cuda/bin:/home/ne0/bin:/home/ne0/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin'
export ROSLISP_PACKAGE_DIRECTORIES='/home/ne0/m4_csv2tf/src/m4_csv2tf/cmake-build-debug/devel/share/common-lisp'
export ROS_PACKAGE_PATH="/home/ne0/m4_csv2tf/src/m4_csv2tf:$ROS_PACKAGE_PATH"