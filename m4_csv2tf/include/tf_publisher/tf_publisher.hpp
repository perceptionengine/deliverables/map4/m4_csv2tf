// Header guard
#ifndef TF_PUBLISHER_H
#define TF_PUBLISHER_H

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>
#include <trajectory_handler/trajectory_handler.h>

class TFPublisher
{
public:
  TFPublisher();

private:
  ros::NodeHandle node_handle_;
  ros::Subscriber points_sub_;

  std::string map4_engine_csv_;
  std::string points_topic_;

  std::string origin_frame_id_;
  std::string localizer_frame_id_;
  std::string base_link_frame_id_;

  bool publish_baselink_;

  TrajectoryHandler traj_handler_;

  void pointCallback(const sensor_msgs::PointCloud2& point_msg);
};

#endif  // TF_PUBLISHER_H
