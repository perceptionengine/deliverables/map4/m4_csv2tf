#include "tf_publisher/tf_publisher.hpp"

TFPublisher::TFPublisher() : node_handle_("~")
{
  ROS_INFO("Inititalizing TF Publisher node ...");

  node_handle_.param<std::string>("map4_engine_csv", map4_engine_csv_, "/home/map4_engine/temp.csv");
  ROS_INFO("MapIV Engine output: %s", map4_engine_csv_.c_str());
  node_handle_.param<std::string>("points_topic", points_topic_, "/points_raw");
  ROS_INFO("Input point cloud topic: %s", points_topic_.c_str());
  node_handle_.param<std::string>("origin_frame_id", origin_frame_id_, "world");
  ROS_INFO("Origin frame ID: %s", origin_frame_id_.c_str());
  node_handle_.param<std::string>("localizer_frame_id", localizer_frame_id_, "velodyne");
  ROS_INFO("Localizer frame ID: %s", localizer_frame_id_.c_str());
  node_handle_.param<bool>("publish_baselink", publish_baselink_, false);
  if (publish_baselink_)
  {
    node_handle_.param<std::string>("base_link_frame_id", base_link_frame_id_, "base_link");
    ROS_INFO("Base link frame ID: %s", base_link_frame_id_.c_str());
  }

  traj_handler_.readCSV(map4_engine_csv_);

  points_sub_ = node_handle_.subscribe(points_topic_, 100, &TFPublisher::pointCallback, this);
}

void TFPublisher::pointCallback(const sensor_msgs::PointCloud2& point_msg)
{
  ros::Time msg_stamp = point_msg.header.stamp;
  TrajectoryPoint closest_trajectory_point = traj_handler_.getClosestOutput(msg_stamp.toSec());

  tf::StampedTransform localizer_tf;
  localizer_tf.stamp_ = closest_trajectory_point.header.stamp;
  localizer_tf.frame_id_ = origin_frame_id_;
  localizer_tf.child_frame_id_ = localizer_frame_id_;
  localizer_tf.setData(closest_trajectory_point.transform);

  static tf::TransformBroadcaster localizer_broadcaster;
  localizer_broadcaster.sendTransform(localizer_tf);

  ROS_INFO_STREAM("Broadcasting localizer TF at: " << localizer_tf.stamp_ << " " << origin_frame_id_ << "," << localizer_frame_id_);

  if (publish_baselink_)
  {
    tf::StampedTransform base_link_tf;
    base_link_tf.stamp_ = closest_trajectory_point.header.stamp;
    base_link_tf.frame_id_ = origin_frame_id_;
    base_link_tf.child_frame_id_ = base_link_frame_id_;
    base_link_tf.setData(pose_conversions::pose2TF(closest_trajectory_point.base_link));

    static tf::TransformBroadcaster base_link_broadcaster;
    base_link_broadcaster.sendTransform(base_link_tf);

    ROS_INFO_STREAM("Broadcasting Base link TF at: " << base_link_tf.stamp_);
  }
}
