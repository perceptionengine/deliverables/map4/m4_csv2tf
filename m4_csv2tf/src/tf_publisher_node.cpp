#include "tf_publisher/tf_publisher.hpp"

int main(int argc, char** argv)
{
  ros::init(argc, argv, "tf_publisher");
  TFPublisher node;
  ros::spin();
  return 0;
}
