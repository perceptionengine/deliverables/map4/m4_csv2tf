// Header guard
#ifndef POSE_CONVERSIONS_H
#define POSE_CONVERSIONS_H

#include "pose.h"
#include <string>
#include <yaml-cpp/yaml.h>
#include <tf/transform_datatypes.h>

namespace pose_conversions
{
// Pose <---> TF
Pose TF2Pose(tf::Transform tf);
tf::Transform pose2TF(Pose pose);
// Pose <---> 4x4 Matrix
Eigen::Matrix4f pose2Matrix(Pose pose);
Pose matrix2Pose(Eigen::Matrix4f matrix);
// Get a transformation matrix from base_link to localizer
Eigen::Matrix4f getB2LMatrix(const std::string& config_file);
// Get a transformation matrix from localizer to base_link
Eigen::Matrix4f getL2BMatrix(const std::string& config_file);
}  // namespace pose_conversions

#endif  // POSE_CONVERSIONS_H
