#include "pose/pose_conversions.h"

Pose pose_conversions::TF2Pose(tf::Transform tf)
{
  Pose out_pose;
  out_pose.x = tf.getOrigin().x();
  out_pose.y = tf.getOrigin().y();
  out_pose.z = tf.getOrigin().z();
  tf::Matrix3x3(tf.getRotation()).getRPY(out_pose.roll, out_pose.pitch, out_pose.yaw);
  return out_pose;
}

tf::Transform pose_conversions::pose2TF(Pose pose)
{
  tf::Vector3 origin;
  tf::Quaternion rotation;
  origin.setValue(pose.x, pose.y, pose.z);
  rotation.setRPY(pose.roll, pose.pitch, pose.yaw);

  tf::Transform out_tf;
  out_tf.setOrigin(origin);
  out_tf.setRotation(rotation);
  return out_tf;
}

Eigen::Matrix4f pose_conversions::pose2Matrix(Pose pose)
{
  Eigen::AngleAxisf init_rotation_x(pose.roll, Eigen::Vector3f::UnitX());
  Eigen::AngleAxisf init_rotation_y(pose.pitch, Eigen::Vector3f::UnitY());
  Eigen::AngleAxisf init_rotation_z(pose.yaw, Eigen::Vector3f::UnitZ());
  Eigen::Translation3f init_translation(pose.x, pose.y, pose.z);
  Eigen::Matrix4f matrix = (init_translation * init_rotation_z * init_rotation_y * init_rotation_x).matrix();
  return matrix;
}

Pose pose_conversions::matrix2Pose(Eigen::Matrix4f matrix)
{
  Pose pose;
  tf::Matrix3x3 mat;
  mat.setValue(static_cast<double>(matrix(0, 0)), static_cast<double>(matrix(0, 1)), static_cast<double>(matrix(0, 2)),
               static_cast<double>(matrix(1, 0)), static_cast<double>(matrix(1, 1)), static_cast<double>(matrix(1, 2)),
               static_cast<double>(matrix(2, 0)), static_cast<double>(matrix(2, 1)), static_cast<double>(matrix(2, 2)));
  pose.x = matrix(0, 3);
  pose.y = matrix(1, 3);
  pose.z = matrix(2, 3);
  mat.getRPY(pose.roll, pose.pitch, pose.yaw, 1);
  return pose;
}

Eigen::Matrix4f pose_conversions::getB2LMatrix(const std::string& config_file)
{
  double _tf_x, _tf_y, _tf_z, _tf_roll, _tf_pitch, _tf_yaw;

  try
  {
    YAML::Node conf = YAML::LoadFile(config_file);
    _tf_x = conf["tf_x"].as<double>();
    _tf_y = conf["tf_y"].as<double>();
    _tf_z = conf["tf_z"].as<double>();
    _tf_roll = conf["tf_roll"].as<double>();
    _tf_pitch = conf["tf_pitch"].as<double>();
    _tf_yaw = conf["tf_yaw"].as<double>();
  }
  catch (YAML::Exception& e)
  {
    std::cerr << "\033[1;31mError: " << e.what() << "\033[0m" << std::endl;
    exit(3);
  }

  // tl: translation, rot: rotation
  Eigen::Translation3f tl_btol(_tf_x, _tf_y, _tf_z);
  Eigen::AngleAxisf rot_x_btol(_tf_roll, Eigen::Vector3f::UnitX());
  Eigen::AngleAxisf rot_y_btol(_tf_pitch, Eigen::Vector3f::UnitY());
  Eigen::AngleAxisf rot_z_btol(_tf_yaw, Eigen::Vector3f::UnitZ());
  Eigen::Matrix4f tf_btol = (tl_btol * rot_z_btol * rot_y_btol * rot_x_btol).matrix();
  return tf_btol;
}

Eigen::Matrix4f pose_conversions::getL2BMatrix(const std::string& config_file)
{
  return getB2LMatrix(config_file).inverse();
}
