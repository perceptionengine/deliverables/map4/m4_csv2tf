// Header guard
#ifndef EAGLEYE_TRAJECTORY_HANDLER_H
#define EAGLEYE_TRAJECTORY_HANDLER_H

#include <yaml-cpp/yaml.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "eagleye_trajectory_point.h"

class EagleyeTrajectoryHandler
{
public:
  EagleyeTrajectoryHandler();

  // Trajectory data
  std::vector<EagleyeTrajectoryPoint> traj_;

  // Basic functions
  void setTraj(std::vector<EagleyeTrajectoryPoint> in_traj);
  void readCSV(std::string log_name);
  void writeCSV(std::string file_name);
  void addTrajectoryPoint(EagleyeTrajectoryPoint trajectory_point);
  EagleyeTrajectoryPoint getClosestOutput(double in_time);

private:
  // Temp log file
  std::ofstream temp_log_file;
  // Helping functions
  ros::Time nannoSecStr2ROSTime(std::string& input);
  std::vector<std::string> split(std::string& input, char delimiter);
};
#endif  // EAGLEYE_TRAJECTORY_HANDLER_H
