#ifndef EAGLEYE_TRAJECTORY_POINT_H
#define EAGLEYE_TRAJECTORY_POINT_H

#include <Eigen/Dense>
#include <std_msgs/Header.h>
#include <geometry_msgs/Twist.h>

struct EagleyeTrajectoryPoint
{
  // Gerneral
  std_msgs::Header header;
  // Position
  int eagleye_status;
  double lat;
  double lon;
  double alt;
  Eigen::Matrix3f position_cov;
  // Orienation
  double roll;
  double pitch;
  double yaw;  // East North UP (MapIV_yaw = M_PI/2.0 - yaw)
  Eigen::Matrix3f orientation_cov;
  // Twist
  geometry_msgs::Twist twist;
  Eigen::Matrix3f linear_twist_cov;
  Eigen::Matrix3f angular_twist_cov;
  // Acceleration
  double accel_x;
  double accel_y;
  double accel_z;
  Eigen::Matrix3f accel_cov;
  // F9P
  int f9p_status;
  double f9p_lat;
  double f9p_lon;
  double f9p_alt;
  Eigen::Matrix3f f9p_cov;
};

#endif  // EAGLEYE_TRAJECTORY_POINT_H
