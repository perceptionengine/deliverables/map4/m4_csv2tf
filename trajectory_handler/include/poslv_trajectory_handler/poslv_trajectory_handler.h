#ifndef POSLV_TRAJECTORY_HANDLER_H
#define POSLV_TRAJECTORY_HANDLER_H

#include <vector>
#include <string>

#include "poslv_trajectory_point.h"

class PoslvTrajectoryHandler
{
public:
  PoslvTrajectoryHandler();

  // Trajectory data
  std::vector<PoslvTrajectoryPoint> traj_;

  // Basic functions
  void setTraj(std::vector<PoslvTrajectoryPoint> in_traj);
  void readCSV(std::string log_name);
  void writeCSV(std::string file_name);
  void addTrajectoryPoint(PoslvTrajectoryPoint trajectory_point);
  PoslvTrajectoryPoint getClosestOutput(double in_time);

private:
  std::vector<std::string> split(std::string& input, char delimiter);
  ros::Time stringToROSTime(std::string& input);
};

#endif
