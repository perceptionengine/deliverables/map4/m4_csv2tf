#ifndef POSLV_TRAJECTORY_POINT_H
#define POSLV_TRAJECTORY_POINT_H

#include <std_msgs/Header.h>

struct PoslvTrajectoryPoint
{
  // General
  double tow;
  std_msgs::Header header;

  // Position
  double lat;
  double lon;
  double alt;
  // Orientation
  double roll;
  double pitch;
  double yaw;

  // Velocity
  double east_vel;
  double north_vel;
  double up_vel;
};

#endif
