// Header guard
#ifndef TRAJECTORY_HANDLER_H
#define TRAJECTORY_HANDLER_H

#include <yaml-cpp/yaml.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "trajectory_point.h"
#include <pose/pose.h>
#include <pose/pose_conversions.h>

class TrajectoryHandler
{
public:
  TrajectoryHandler();

  // Trajectory data
  std::vector<TrajectoryPoint> traj;

  // Basic functions
  void setTraj(std::vector<TrajectoryPoint> in_traj);
  void readCSV(std::string log_name);
  void writeCSV(std::string file_name);
  void addTrajectoryPoint(TrajectoryPoint trajectory_point);
  void fillTwist();
  TrajectoryPoint getClosestOutput(double in_time);
  void setTFMatrix(std::string config_file);

  // Post-processing functions
  Pose projectPose(Pose current_pose, double current_stamp, double estimated_last_point_stamp);
  std::vector<TrajectoryPoint> reverseTrajectory(std::vector<TrajectoryPoint> raw_output,
                                                 const std::string& ndt_params);
  // Other parameters
  bool is_twist_filled_;

private:
  // for output
  int print_point_;
  // Temp log file
  std::ofstream temp_log_file;
  // Helping functions
  ros::Time str2ROSTime(std::string& input);
  std::vector<std::string> split(std::string& input, char delimiter);
  Eigen::Matrix4f l2b_matrix_;
};
#endif  // TRAJECTORY_HANDLER_H
