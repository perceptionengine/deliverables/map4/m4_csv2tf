#ifndef TRAJECTORY_POINT_H
#define TRAJECTORY_POINT_H

#include <std_msgs/Header.h>
#include <tf/transform_datatypes.h>
#include <geometry_msgs/Twist.h>
#include <pose/pose.h>

struct TrajectoryPoint
{
  // Gerneral
  std_msgs::Header header;
  tf::Transform transform;
  Pose localizer;
  Pose base_link;
  geometry_msgs::Twist twist;
  // Specific to NDT
  double fitness_score;
  double transformation_probability;
  // For SLAM failure detection
  int reliability = 0;  // 1 Good results, 0 Bad reesults
  // Specific to Trajectory fusion
  bool forward_usable;
  bool backward_usable;
  Pose ekf_speed;
};

#endif  // TRAJECTORY_POINT_H
