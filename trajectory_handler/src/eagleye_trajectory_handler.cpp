#include "eagleye_trajectory_handler/eagleye_trajectory_handler.h"

EagleyeTrajectoryHandler::EagleyeTrajectoryHandler()
{
}

void EagleyeTrajectoryHandler::setTraj(std::vector<EagleyeTrajectoryPoint> in_traj)
{
  traj_ = in_traj;
}

void EagleyeTrajectoryHandler::readCSV(std::string log_name)
{
  // Load the Eagleye output file
  std::ifstream ifs(log_name);
  if (!ifs)
  {
    std::cerr << "\033[1;31mError: Cannot open Eagleye output file: " << log_name << "\033[0m" << std::endl;
    exit(2);
  }

  // Parse the contents
  int count = 0;
  std::string line;
  std::vector<EagleyeTrajectoryPoint> input_traj;
  while (getline(ifs, line))
  {
    // Skip the header
    if (count > 0)
    {
      EagleyeTrajectoryPoint item;
      // Separate values in each line
      std::vector<std::string> str_vec = split(line, ',');
      // // Gerneral
      // std_msgs::Header header;
      item.header.stamp = nannoSecStr2ROSTime(str_vec.at(0));
      item.header.seq = count - 1;
      item.header.frame_id = "/gnss";
      // // Position
      // int eagleye_status;
      item.eagleye_status = std::stoi(str_vec.at(13));
      // double lat;
      // double lon;
      // double alt;
      item.lat = std::stod(str_vec.at(1));
      item.lon = std::stod(str_vec.at(2));
      item.alt = std::stod(str_vec.at(3));
      // Eigen::Matrix3f position_cov;
      // //  lat lon alt
      // lat  0   1   2
      // lon  3   4   5
      // alt  6   7   8
      // Double check this order again!
      item.position_cov(0, 0) = std::stof(str_vec.at(4));
      item.position_cov(0, 1) = std::stof(str_vec.at(5));
      item.position_cov(0, 2) = std::stof(str_vec.at(6));
      item.position_cov(1, 0) = std::stof(str_vec.at(7));
      item.position_cov(1, 1) = std::stof(str_vec.at(8));
      item.position_cov(1, 2) = std::stof(str_vec.at(9));
      item.position_cov(2, 0) = std::stof(str_vec.at(10));
      item.position_cov(2, 1) = std::stof(str_vec.at(11));
      item.position_cov(2, 2) = std::stof(str_vec.at(12));
      // // Orienation
      // double roll (not implemneted yet);
      // double pitch (not implemneted yet);
      // double yaw; // wrt. North
      item.roll = std::stod(str_vec.at(50));
      item.pitch = std::stod(str_vec.at(51));
      item.yaw = std::stod(str_vec.at(52));
      // Eigen::Matrix3f orientation_cov;
      // //   roll pitch yaw
      // roll   0   1   2
      // pitch  3   4   5
      // yaw    6   7   8
      // Double check this order again!
      item.orientation_cov(0, 0) = std::stof(str_vec.at(53));
      item.orientation_cov(0, 1) = std::stof(str_vec.at(54));
      item.orientation_cov(0, 2) = std::stof(str_vec.at(55));
      item.orientation_cov(1, 0) = std::stof(str_vec.at(56));
      item.orientation_cov(1, 1) = std::stof(str_vec.at(57));
      item.orientation_cov(1, 2) = std::stof(str_vec.at(58));
      item.orientation_cov(2, 0) = std::stof(str_vec.at(59));
      item.orientation_cov(2, 1) = std::stof(str_vec.at(60));
      item.orientation_cov(2, 2) = std::stof(str_vec.at(61));
      // // Twist
      // geometry_msgs::Twist twist;
      item.twist.linear.x = std::stof(str_vec.at(14));
      item.twist.linear.y = std::stof(str_vec.at(15));
      item.twist.linear.z = std::stof(str_vec.at(16));
      item.twist.angular.x = std::stof(str_vec.at(17));
      item.twist.angular.y = std::stof(str_vec.at(18));
      item.twist.angular.z = std::stof(str_vec.at(19));
      // Eigen::Matrix3f linear_twist_cov;
      // //     lin_x lin_y lin_z
      // lin_x    0   1   2
      // lin_y    3   4   5
      // lin_z    6   7   8
      // Double check this order again!
      item.linear_twist_cov(0, 0) = std::stof(str_vec.at(20));
      item.linear_twist_cov(0, 1) = std::stof(str_vec.at(21));
      item.linear_twist_cov(0, 2) = std::stof(str_vec.at(22));
      item.linear_twist_cov(1, 0) = std::stof(str_vec.at(23));
      item.linear_twist_cov(1, 1) = std::stof(str_vec.at(24));
      item.linear_twist_cov(1, 2) = std::stof(str_vec.at(25));
      item.linear_twist_cov(2, 0) = std::stof(str_vec.at(26));
      item.linear_twist_cov(2, 1) = std::stof(str_vec.at(27));
      item.linear_twist_cov(2, 2) = std::stof(str_vec.at(28));
      // Eigen::Matrix3f angular_twist_cov;
      // //     ang_x ang_y ang_z
      // ang_x    0   1   2
      // ang_y    3   4   5
      // ang_z    6   7   8
      // Double check this order again!
      item.angular_twist_cov(0, 0) = std::stof(str_vec.at(29));
      item.angular_twist_cov(0, 1) = std::stof(str_vec.at(30));
      item.angular_twist_cov(0, 2) = std::stof(str_vec.at(31));
      item.angular_twist_cov(1, 0) = std::stof(str_vec.at(32));
      item.angular_twist_cov(1, 1) = std::stof(str_vec.at(33));
      item.angular_twist_cov(1, 2) = std::stof(str_vec.at(34));
      item.angular_twist_cov(2, 0) = std::stof(str_vec.at(35));
      item.angular_twist_cov(2, 1) = std::stof(str_vec.at(36));
      item.angular_twist_cov(2, 2) = std::stof(str_vec.at(37));
      // // Acceleration
      // double accel_x;
      // double accel_y;
      // double accel_z;
      item.accel_x = std::stod(str_vec.at(38));
      item.accel_y = std::stod(str_vec.at(39));
      item.accel_z = std::stod(str_vec.at(40));
      // Eigen::Matrix3f accel_cov;
      // //  x  y  z
      // x  0   1   2
      // y  3   4   5
      // z  6   7   8
      item.accel_cov(0, 0) = std::stof(str_vec.at(41));
      item.accel_cov(0, 1) = std::stof(str_vec.at(42));
      item.accel_cov(0, 2) = std::stof(str_vec.at(43));
      item.accel_cov(1, 0) = std::stof(str_vec.at(44));
      item.accel_cov(1, 1) = std::stof(str_vec.at(45));
      item.accel_cov(1, 2) = std::stof(str_vec.at(46));
      item.accel_cov(2, 0) = std::stof(str_vec.at(47));
      item.accel_cov(2, 1) = std::stof(str_vec.at(48));
      item.accel_cov(2, 2) = std::stof(str_vec.at(49));
      // // F9P
      // int f9p_status;
      item.f9p_status = std::stoi(str_vec.at(74));
      // double f9p_lat;
      // double f9p_lon;
      // double f9p_alt;
      item.f9p_lat = std::stod(str_vec.at(62));
      item.f9p_lon = std::stod(str_vec.at(63));
      item.f9p_alt = std::stod(str_vec.at(64));
      // Eigen::Matrix3f f9p_cov;
      // //  lat lon alt
      // lat  0   1   2
      // lon  3   4   5
      // alt  6   7   8
      // Double check this order again!
      item.f9p_cov(0, 0) = std::stof(str_vec.at(65));
      item.f9p_cov(0, 1) = std::stof(str_vec.at(66));
      item.f9p_cov(0, 2) = std::stof(str_vec.at(67));
      item.f9p_cov(1, 0) = std::stof(str_vec.at(68));
      item.f9p_cov(1, 1) = std::stof(str_vec.at(69));
      item.f9p_cov(1, 2) = std::stof(str_vec.at(70));
      item.f9p_cov(2, 0) = std::stof(str_vec.at(71));
      item.f9p_cov(2, 1) = std::stof(str_vec.at(72));
      item.f9p_cov(2, 2) = std::stof(str_vec.at(73));
      // Store the output in a vector
      input_traj.push_back(item);
    }
    count++;
  }
  // Set the trajectory data
  setTraj(input_traj);
}

void EagleyeTrajectoryHandler::writeCSV(std::string file_name)
{
  if (traj_.size() == 0)
  {
    std::cerr << "\033[1;31mError: The input trajectory has not been set! \033[0m" << std::endl;
    exit(4);
  }
  else
  {
    // Set log file name.
    std::ofstream ofs;
    ofs.open(file_name.c_str(), std::ios::app);
    if (!ofs.is_open())
    {
      std::cerr << "\033[1;31mError: Could not open " << file_name << ". \033[0m" << std::endl;
      exit(2);
    }

    // Write CSV file header
    // clang-format off
    ofs << "timestamp" << ","
        << "eagleye_llh.latitude" << ","
        << "eagleye_llh.longitude" << ","
        << "eagleye_llh.altitude" << ","
        << "eagleye_llh.orientation_covariance[0]" << ","
        << "eagleye_llh.orientation_covariance[1]" << ","
        << "eagleye_llh.orientation_covariance[2]" << ","
        << "eagleye_llh.orientation_covariance[3]" << ","
        << "eagleye_llh.orientation_covariance[4]" << ","
        << "eagleye_llh.orientation_covariance[5]" << ","
        << "eagleye_llh.orientation_covariance[6]" << ","
        << "eagleye_llh.orientation_covariance[7]" << ","
        << "eagleye_llh.orientation_covariance[8]" << ","
        << "eagleye_llh.status" << ","
        << "eagleye_twist.linear.x" << ","
        << "eagleye_twist.linear.y" << ","
        << "eagleye_twist.linear.z" << ","
        << "eagleye_twist.angular.x" << ","
        << "eagleye_twist.angular.y" << ","
        << "eagleye_twist.angular.z" << ","
        << "eagleye_twist.orientation_covariance[0]" << ","
        << "eagleye_twist.orientation_covariance[1]" << ","
        << "eagleye_twist.orientation_covariance[2]" << ","
        << "eagleye_twist.orientation_covariance[3]" << ","
        << "eagleye_twist.orientation_covariance[4]" << ","
        << "eagleye_twist.orientation_covariance[5]" << ","
        << "eagleye_twist.orientation_covariance[6]" << ","
        << "eagleye_twist.orientation_covariance[7]" << ","
        << "eagleye_twist.orientation_covariance[8]" << ","
        << "eagleye_twist.orientation_covariance[9]" << ","
        << "eagleye_twist.orientation_covariance[10]" << ","
        << "eagleye_twist.orientation_covariance[11]" << ","
        << "eagleye_twist.orientation_covariance[12]" << ","
        << "eagleye_twist.orientation_covariance[13]" << ","
        << "eagleye_twist.orientation_covariance[14]" << ","
        << "eagleye_twist.orientation_covariance[15]" << ","
        << "eagleye_twist.orientation_covariance[16]" << ","
        << "eagleye_twist.orientation_covariance[17]" << ","
        << "eagleye_acceleration.x" << ","
        << "eagleye_acceleration.y" << ","
        << "eagleye_acceleration.z" << ","
        << "eagleye_acceleration.orientation_covariance[0]" << ","
        << "eagleye_acceleration.orientation_covariance[1]" << ","
        << "eagleye_acceleration.orientation_covariance[2]" << ","
        << "eagleye_acceleration.orientation_covariance[3]" << ","
        << "eagleye_acceleration.orientation_covariance[4]" << ","
        << "eagleye_acceleration.orientation_covariance[5]" << ","
        << "eagleye_acceleration.orientation_covariance[6]" << ","
        << "eagleye_acceleration.orientation_covariance[7]" << ","
        << "eagleye_acceleration.orientation_covariance[8]" << ","
        << "eagleye_posture.roll" << ","
        << "eagleye_posture.pitch" << ","
        << "eagleye_posture.yaw" << ","
        << "eagleye_posture.orientation_covariance[0]" << ","
        << "eagleye_posture.orientation_covariance[1]" << ","
        << "eagleye_posture.orientation_covariance[2]" << ","
        << "eagleye_posture.orientation_covariance[3]" << ","
        << "eagleye_posture.orientation_covariance[4]" << ","
        << "eagleye_posture.orientation_covariance[5]" << ","
        << "eagleye_posture.orientation_covariance[6]" << ","
        << "eagleye_posture.orientation_covariance[7]" << ","
        << "eagleye_posture.orientation_covariance[8]" << ","
        << "f9p_llh.latitude" << ","
        << "f9p_llh.longitude" << ","
        << "f9p_llh.altitude" << ","
        << "f9p_llh.orientation_covariance[0]" << ","
        << "f9p_llh.orientation_covariance[1]" << ","
        << "f9p_llh.orientation_covariance[2]" << ","
        << "f9p_llh.orientation_covariance[3]" << ","
        << "f9p_llh.orientation_covariance[4]" << ","
        << "f9p_llh.orientation_covariance[5]" << ","
        << "f9p_llh.orientation_covariance[6]" << ","
        << "f9p_llh.orientation_covariance[7]" << ","
        << "f9p_llh.orientation_covariance[8]" << ","
        << "f9p_llh.status"
        << std::endl;

    for (auto output_iter = traj_.begin(); output_iter != traj_.end(); ++output_iter)
    {
          // << "timestamp" << ","
      ofs << output_iter->header.stamp 
          // << "eagleye_llh.latitude" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->lat
          // << "eagleye_llh.longitude" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->lon
          // << "eagleye_llh.altitude" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->alt
          // << "eagleye_llh.orientation_covariance[0]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->position_cov(0, 0)
          // << "eagleye_llh.orientation_covariance[1]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->position_cov(0, 1)
          // << "eagleye_llh.orientation_covariance[2]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->position_cov(0, 2)
          // << "eagleye_llh.orientation_covariance[3]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->position_cov(1, 0)
          // << "eagleye_llh.orientation_covariance[4]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->position_cov(1, 1)
          // << "eagleye_llh.orientation_covariance[5]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->position_cov(1, 2)
          // << "eagleye_llh.orientation_covariance[6]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->position_cov(2, 0)
          // << "eagleye_llh.orientation_covariance[7]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->position_cov(2, 1)
          // << "eagleye_llh.orientation_covariance[8]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->position_cov(2, 2)
          // << "eagleye_llh.status" << ","
          << "," << output_iter->eagleye_status
          // << "eagleye_twist.linear.x" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->twist.linear.x
          // << "eagleye_twist.linear.y" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->twist.linear.y
          // << "eagleye_twist.linear.z" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->twist.linear.z
          // << "eagleye_twist.angular.x" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->twist.angular.x
          // << "eagleye_twist.angular.y" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->twist.angular.y
          // << "eagleye_twist.angular.z" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->twist.angular.z
          // << "eagleye_twist.orientation_covariance[0]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->linear_twist_cov(0,0)
          // << "eagleye_twist.orientation_covariance[1]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->linear_twist_cov(0,1)
          // << "eagleye_twist.orientation_covariance[2]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->linear_twist_cov(0,2)
          // << "eagleye_twist.orientation_covariance[3]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->linear_twist_cov(1,0)
          // << "eagleye_twist.orientation_covariance[4]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->linear_twist_cov(1,1)
          // << "eagleye_twist.orientation_covariance[5]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->linear_twist_cov(1,2)
          // << "eagleye_twist.orientation_covariance[6]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->linear_twist_cov(2,0)
          // << "eagleye_twist.orientation_covariance[7]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->linear_twist_cov(2,1)
          // << "eagleye_twist.orientation_covariance[8]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->linear_twist_cov(2,2)
          // << "eagleye_twist.orientation_covariance[9]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->angular_twist_cov(0,0)
          // << "eagleye_twist.orientation_covariance[10]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->angular_twist_cov(0,1)
          // << "eagleye_twist.orientation_covariance[11]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->angular_twist_cov(0,2)
          // << "eagleye_twist.orientation_covariance[12]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->angular_twist_cov(1,0)
          // << "eagleye_twist.orientation_covariance[13]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->angular_twist_cov(1,1)
          // << "eagleye_twist.orientation_covariance[14]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->angular_twist_cov(1,2)
          // << "eagleye_twist.orientation_covariance[15]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->angular_twist_cov(2,0)
          // << "eagleye_twist.orientation_covariance[16]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->angular_twist_cov(2,1)
          // << "eagleye_twist.orientation_covariance[17]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->angular_twist_cov(2,2)
          // << "eagleye_acceleration.x" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->accel_x
          // << "eagleye_acceleration.y" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->accel_y
          // << "eagleye_acceleration.z" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->accel_z
          // << "eagleye_acceleration.orientation_covariance[0]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->accel_cov(0,0)
          // << "eagleye_acceleration.orientation_covariance[1]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->accel_cov(0,1)
          // << "eagleye_acceleration.orientation_covariance[2]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->accel_cov(0,2)
          // << "eagleye_acceleration.orientation_covariance[3]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->accel_cov(1,0)
          // << "eagleye_acceleration.orientation_covariance[4]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->accel_cov(1,1)
          // << "eagleye_acceleration.orientation_covariance[5]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->accel_cov(1,2)
          // << "eagleye_acceleration.orientation_covariance[6]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->accel_cov(2,0)
          // << "eagleye_acceleration.orientation_covariance[7]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->accel_cov(2,1)
          // << "eagleye_acceleration.orientation_covariance[8]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->accel_cov(2,2)
          // << "eagleye_posture.roll" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->roll
          // << "eagleye_posture.pitch" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->pitch
          // << "eagleye_posture.yaw" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->yaw
          // << "eagleye_posture.orientation_covariance[0]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->position_cov(0, 0)
          // << "eagleye_posture.orientation_covariance[1]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->position_cov(0, 1)
          // << "eagleye_posture.orientation_covariance[2]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->position_cov(0, 2)
          // << "eagleye_posture.orientation_covariance[3]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->position_cov(1, 0)
          // << "eagleye_posture.orientation_covariance[4]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->position_cov(1, 1)
          // << "eagleye_posture.orientation_covariance[5]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->position_cov(1, 2)
          // << "eagleye_posture.orientation_covariance[6]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->position_cov(2, 0)
          // << "eagleye_posture.orientation_covariance[7]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->position_cov(2, 1)
          // << "eagleye_posture.orientation_covariance[8]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->position_cov(2, 2)
          // << "f9p_llh.latitude" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->lat
          // << "f9p_llh.longitude" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->lon
          // << "f9p_llh.altitude" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->alt
          // << "f9p_llh.orientation_covariance[0]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->f9p_cov(0, 0)
          // << "f9p_llh.orientation_covariance[1]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->f9p_cov(0, 1)
          // << "f9p_llh.orientation_covariance[2]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->f9p_cov(0, 2)
          // << "f9p_llh.orientation_covariance[3]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->f9p_cov(1, 0)
          // << "f9p_llh.orientation_covariance[4]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->f9p_cov(1, 1)
          // << "f9p_llh.orientation_covariance[5]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->f9p_cov(1, 2)
          // << "f9p_llh.orientation_covariance[6]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->f9p_cov(2, 0)
          // << "f9p_llh.orientation_covariance[7]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->f9p_cov(2, 1)
          // << "f9p_llh.orientation_covariance[8]" << ","
          << "," << std::fixed << std::setprecision(5) << output_iter->f9p_cov(2, 2)
          // << "f9p_llh.status"
          << "," << output_iter->f9p_status
          << std::endl;
    }
    // clang-format on
    ofs.close();
  }
}

void EagleyeTrajectoryHandler::addTrajectoryPoint(EagleyeTrajectoryPoint trajectory_point)
{
  if (traj_.size() == 0)
  {
    // Set a temporary log file name.
    temp_log_file.open("/tmp/temp_eagleye_trajectory.csv", std::ios::app);
    // Write CSV file header
    // clang-format off
    temp_log_file << "timestamp" << ","
                  << "eagleye_llh.latitude" << ","
                  << "eagleye_llh.longitude" << ","
                  << "eagleye_llh.altitude" << ","
                  << "eagleye_llh.orientation_covariance[0]" << ","
                  << "eagleye_llh.orientation_covariance[1]" << ","
                  << "eagleye_llh.orientation_covariance[2]" << ","
                  << "eagleye_llh.orientation_covariance[3]" << ","
                  << "eagleye_llh.orientation_covariance[4]" << ","
                  << "eagleye_llh.orientation_covariance[5]" << ","
                  << "eagleye_llh.orientation_covariance[6]" << ","
                  << "eagleye_llh.orientation_covariance[7]" << ","
                  << "eagleye_llh.orientation_covariance[8]" << ","
                  << "eagleye_llh.status" << ","
                  << "eagleye_twist.linear.x" << ","
                  << "eagleye_twist.linear.y" << ","
                  << "eagleye_twist.linear.z" << ","
                  << "eagleye_twist.angular.x" << ","
                  << "eagleye_twist.angular.y" << ","
                  << "eagleye_twist.angular.z" << ","
                  << "eagleye_twist.orientation_covariance[0]" << ","
                  << "eagleye_twist.orientation_covariance[1]" << ","
                  << "eagleye_twist.orientation_covariance[2]" << ","
                  << "eagleye_twist.orientation_covariance[3]" << ","
                  << "eagleye_twist.orientation_covariance[4]" << ","
                  << "eagleye_twist.orientation_covariance[5]" << ","
                  << "eagleye_twist.orientation_covariance[6]" << ","
                  << "eagleye_twist.orientation_covariance[7]" << ","
                  << "eagleye_twist.orientation_covariance[8]" << ","
                  << "eagleye_twist.orientation_covariance[9]" << ","
                  << "eagleye_twist.orientation_covariance[10]" << ","
                  << "eagleye_twist.orientation_covariance[11]" << ","
                  << "eagleye_twist.orientation_covariance[12]" << ","
                  << "eagleye_twist.orientation_covariance[13]" << ","
                  << "eagleye_twist.orientation_covariance[14]" << ","
                  << "eagleye_twist.orientation_covariance[15]" << ","
                  << "eagleye_twist.orientation_covariance[16]" << ","
                  << "eagleye_twist.orientation_covariance[17]" << ","
                  << "eagleye_acceleration.x" << ","
                  << "eagleye_acceleration.y" << ","
                  << "eagleye_acceleration.z" << ","
                  << "eagleye_acceleration.orientation_covariance[0]" << ","
                  << "eagleye_acceleration.orientation_covariance[1]" << ","
                  << "eagleye_acceleration.orientation_covariance[2]" << ","
                  << "eagleye_acceleration.orientation_covariance[3]" << ","
                  << "eagleye_acceleration.orientation_covariance[4]" << ","
                  << "eagleye_acceleration.orientation_covariance[5]" << ","
                  << "eagleye_acceleration.orientation_covariance[6]" << ","
                  << "eagleye_acceleration.orientation_covariance[7]" << ","
                  << "eagleye_acceleration.orientation_covariance[8]" << ","
                  << "eagleye_posture.roll" << ","
                  << "eagleye_posture.pitch" << ","
                  << "eagleye_posture.yaw" << ","
                  << "eagleye_posture.orientation_covariance[0]" << ","
                  << "eagleye_posture.orientation_covariance[1]" << ","
                  << "eagleye_posture.orientation_covariance[2]" << ","
                  << "eagleye_posture.orientation_covariance[3]" << ","
                  << "eagleye_posture.orientation_covariance[4]" << ","
                  << "eagleye_posture.orientation_covariance[5]" << ","
                  << "eagleye_posture.orientation_covariance[6]" << ","
                  << "eagleye_posture.orientation_covariance[7]" << ","
                  << "eagleye_posture.orientation_covariance[8]" << ","
                  << "f9p_llh.latitude" << ","
                  << "f9p_llh.longitude" << ","
                  << "f9p_llh.altitude" << ","
                  << "f9p_llh.orientation_covariance[0]" << ","
                  << "f9p_llh.orientation_covariance[1]" << ","
                  << "f9p_llh.orientation_covariance[2]" << ","
                  << "f9p_llh.orientation_covariance[3]" << ","
                  << "f9p_llh.orientation_covariance[4]" << ","
                  << "f9p_llh.orientation_covariance[5]" << ","
                  << "f9p_llh.orientation_covariance[6]" << ","
                  << "f9p_llh.orientation_covariance[7]" << ","
                  << "f9p_llh.orientation_covariance[8]" << ","
                  << "f9p_llh.status"
                  << std::endl;
  }
  else
  {
                  // << "timestamp" << ","
    temp_log_file << trajectory_point.header.stamp << ","
                  // << "eagleye_llh.latitude" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.lat
                  // << "eagleye_llh.longitude" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.lon
                  // << "eagleye_llh.altitude" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.alt
                  // << "eagleye_llh.orientation_covariance[0]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.position_cov(0, 0)
                  // << "eagleye_llh.orientation_covariance[1]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.position_cov(0, 1)
                  // << "eagleye_llh.orientation_covariance[2]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.position_cov(0, 2)
                  // << "eagleye_llh.orientation_covariance[3]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.position_cov(1, 0)
                  // << "eagleye_llh.orientation_covariance[4]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.position_cov(1, 1)
                  // << "eagleye_llh.orientation_covariance[5]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.position_cov(1, 2)
                  // << "eagleye_llh.orientation_covariance[6]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.position_cov(2, 0)
                  // << "eagleye_llh.orientation_covariance[7]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.position_cov(2, 1)
                  // << "eagleye_llh.orientation_covariance[8]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.position_cov(2, 2)
                  // << "eagleye_llh.status" << ","
                  << "," << trajectory_point.eagleye_status
                  // << "eagleye_twist.linear.x" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.twist.linear.x
                  // << "eagleye_twist.linear.y" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.twist.linear.y
                  // << "eagleye_twist.linear.z" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.twist.linear.z
                  // << "eagleye_twist.angular.x" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.twist.angular.x
                  // << "eagleye_twist.angular.y" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.twist.angular.y
                  // << "eagleye_twist.angular.z" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.twist.angular.z
                  // << "eagleye_twist.orientation_covariance[0]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.linear_twist_cov(0,0)
                  // << "eagleye_twist.orientation_covariance[1]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.linear_twist_cov(0,1)
                  // << "eagleye_twist.orientation_covariance[2]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.linear_twist_cov(0,2)
                  // << "eagleye_twist.orientation_covariance[3]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.linear_twist_cov(1,0)
                  // << "eagleye_twist.orientation_covariance[4]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.linear_twist_cov(1,1)
                  // << "eagleye_twist.orientation_covariance[5]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.linear_twist_cov(1,2)
                  // << "eagleye_twist.orientation_covariance[6]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.linear_twist_cov(2,0)
                  // << "eagleye_twist.orientation_covariance[7]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.linear_twist_cov(2,1)
                  // << "eagleye_twist.orientation_covariance[8]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.linear_twist_cov(2,2)
                  // << "eagleye_twist.orientation_covariance[9]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.angular_twist_cov(0,0)
                  // << "eagleye_twist.orientation_covariance[10]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.angular_twist_cov(0,1)
                  // << "eagleye_twist.orientation_covariance[11]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.angular_twist_cov(0,2)
                  // << "eagleye_twist.orientation_covariance[12]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.angular_twist_cov(1,0)
                  // << "eagleye_twist.orientation_covariance[13]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.angular_twist_cov(1,1)
                  // << "eagleye_twist.orientation_covariance[14]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.angular_twist_cov(1,2)
                  // << "eagleye_twist.orientation_covariance[15]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.angular_twist_cov(2,0)
                  // << "eagleye_twist.orientation_covariance[16]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.angular_twist_cov(2,1)
                  // << "eagleye_twist.orientation_covariance[17]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.angular_twist_cov(2,2)
                  // << "eagleye_acceleration.x" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.accel_x
                  // << "eagleye_acceleration.y" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.accel_y
                  // << "eagleye_acceleration.z" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.accel_z
                  // << "eagleye_acceleration.orientation_covariance[0]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.accel_cov(0,0)
                  // << "eagleye_acceleration.orientation_covariance[1]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.accel_cov(0,1)
                  // << "eagleye_acceleration.orientation_covariance[2]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.accel_cov(0,2)
                  // << "eagleye_acceleration.orientation_covariance[3]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.accel_cov(1,0)
                  // << "eagleye_acceleration.orientation_covariance[4]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.accel_cov(1,1)
                  // << "eagleye_acceleration.orientation_covariance[5]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.accel_cov(1,2)
                  // << "eagleye_acceleration.orientation_covariance[6]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.accel_cov(2,0)
                  // << "eagleye_acceleration.orientation_covariance[7]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.accel_cov(2,1)
                  // << "eagleye_acceleration.orientation_covariance[8]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.accel_cov(2,2)
                  // << "eagleye_posture.roll" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.roll
                  // << "eagleye_posture.pitch" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.pitch
                  // << "eagleye_posture.yaw" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.yaw
                  // << "eagleye_posture.orientation_covariance[0]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.position_cov(0, 0)
                  // << "eagleye_posture.orientation_covariance[1]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.position_cov(0, 1)
                  // << "eagleye_posture.orientation_covariance[2]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.position_cov(0, 2)
                  // << "eagleye_posture.orientation_covariance[3]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.position_cov(1, 0)
                  // << "eagleye_posture.orientation_covariance[4]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.position_cov(1, 1)
                  // << "eagleye_posture.orientation_covariance[5]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.position_cov(1, 2)
                  // << "eagleye_posture.orientation_covariance[6]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.position_cov(2, 0)
                  // << "eagleye_posture.orientation_covariance[7]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.position_cov(2, 1)
                  // << "eagleye_posture.orientation_covariance[8]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.position_cov(2, 2)
                  // << "f9p_llh.latitude" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.lat
                  // << "f9p_llh.longitude" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.lon
                  // << "f9p_llh.altitude" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.alt
                  // << "f9p_llh.orientation_covariance[0]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.f9p_cov(0, 0)
                  // << "f9p_llh.orientation_covariance[1]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.f9p_cov(0, 1)
                  // << "f9p_llh.orientation_covariance[2]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.f9p_cov(0, 2)
                  // << "f9p_llh.orientation_covariance[3]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.f9p_cov(1, 0)
                  // << "f9p_llh.orientation_covariance[4]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.f9p_cov(1, 1)
                  // << "f9p_llh.orientation_covariance[5]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.f9p_cov(1, 2)
                  // << "f9p_llh.orientation_covariance[6]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.f9p_cov(2, 0)
                  // << "f9p_llh.orientation_covariance[7]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.f9p_cov(2, 1)
                  // << "f9p_llh.orientation_covariance[8]" << ","
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.f9p_cov(2, 2)
                  // << "f9p_llh.status"
                  << "," << trajectory_point.f9p_status
                  << std::endl;
    // clang-format on
  }
  traj_.push_back(trajectory_point);
}

EagleyeTrajectoryPoint EagleyeTrajectoryHandler::getClosestOutput(double in_time)
{
  if (traj_.size() == 0)
  {
    std::cerr << "\033[1;31mError: The input trajectory has not been set! \033[0m" << std::endl;
    exit(4);
  }
  else
  {
    // In case the exact match cannot be found
    // These variables are for getting the closest possible pose
    int best_match_id = 0;
    double min_t_diff = -1;
    for (int i = 0; i < traj_.size(); ++i)
    {
      double t_diff = fabs(in_time - traj_[i].header.stamp.toSec());
      if ((t_diff < min_t_diff) || (min_t_diff < 0))
      {
        min_t_diff = t_diff;
        best_match_id = i;
      }
      if (t_diff < 0.01)
      {
        return traj_[i];
      }
    }
    std::cout << "Could not get the output with matching time stamp!!" << '\n';
    std::cout << "Returning the closest possible output!!" << '\n';
    return traj_[best_match_id];
  }
}

ros::Time EagleyeTrajectoryHandler::nannoSecStr2ROSTime(std::string& input)
{
  ros::Time time;
  time.sec = std::stoi(input.substr(0, 10));
  time.nsec = std::stoi(input.substr(10, 9));
  return time;
}

std::vector<std::string> EagleyeTrajectoryHandler::split(std::string& input, char delimiter)
{
  std::istringstream stream(input);
  std::string field;
  std::vector<std::string> result;
  while (std::getline(stream, field, delimiter))
  {
    result.push_back(field);
  }
  return result;
}
