#include <poslv_trajectory_handler/poslv_trajectory_handler.h>

#include <time.h>
#include <fstream>

PoslvTrajectoryHandler::PoslvTrajectoryHandler()
{
}

void PoslvTrajectoryHandler::setTraj(std::vector<PoslvTrajectoryPoint> in_traj)
{
  traj_ = in_traj;
}

void PoslvTrajectoryHandler::readCSV(std::string log_name)
{
  // Load the Poslv output file
  std::ifstream ifs(log_name);
  if (!ifs)
  {
    std::cerr << "\033[1;31mError: Cannot open poslv log file: " << log_name << "\033[0m" << std::endl;
    exit(2);
  }

  // Parse the contents
  int count = 0;
  std::string line;
  std::vector<PoslvTrajectoryPoint> input_traj;

  // Skip the header line
  getline(ifs, line);

  while (getline(ifs, line))
  {
    PoslvTrajectoryPoint item;
    // Separate values in each line
    std::vector<std::string> str_vec = split(line, ',');

    // General
    item.tow = std::stod(str_vec.at(0));
    item.header.stamp = stringToROSTime(str_vec.at(1));
    item.header.seq = count;
    item.header.frame_id = "/gnss";

    // Position
    item.lat = std::stod(str_vec.at(2));
    item.lon = std::stod(str_vec.at(3));
    item.alt = std::stod(str_vec.at(4));

    // Orientation
    item.roll = std::stod(str_vec.at(5)) * (M_PI / 180.0);
    item.pitch = std::stod(str_vec.at(6)) * (M_PI / 180.0);
    item.yaw = std::stod(str_vec.at(7)) * (M_PI / 180.0);

    // Velocity
    item.east_vel = std::stod(str_vec.at(8));
    item.north_vel = std::stod(str_vec.at(9));
    item.up_vel = std::stod(str_vec.at(10));

    input_traj.push_back(item);
    // Count up
    count++;
  }

  // Set the trajectory data
  setTraj(input_traj);
}

void PoslvTrajectoryHandler::writeCSV(std::string file_name)
{
  if (traj_.size() <= 0)
  {
    std::cerr << "\033[1;31mError: The input trajectory has not been set! \033[0m" << std::endl;
    exit(4);
  }

  // Open output file
  std::ofstream ofs(file_name);
  if (!ofs.is_open())
  {
    std::cerr << "\033[1;31mError: Could not open: " << file_name << "\033[0m" << std::endl;
    exit(2);
  }

  // Write header line
  // clang-format off
  ofs << "TOW" << ","
      << "TIME" << ","
      << "LATITUDE" << ","
      << "LONGITUDE" << ","
      << "ORTHOMETRIC_HEIGHT" << ","
      << "ROLL" << ","
      << "PITCH" << ","
      << "HEADING" << ","
      << "EAST_VELOCITY" << ","
      << "NORTH_VELOCITY" << ","
      << "UP_VELOCITY"
      << std::endl;

  for (auto output_iter = traj_.begin(); output_iter != traj_.end(); output_iter++)
  {
    ofs << output_iter->tow << ","
        << output_iter->header.stamp << ","
        << std::fixed << std::setprecision(5) << output_iter->lat << ","
        << std::fixed << std::setprecision(5) << output_iter->lon << ","
        << std::fixed << std::setprecision(5) << output_iter->alt << ","
        << std::fixed << std::setprecision(5) << output_iter->roll << ","
        << std::fixed << std::setprecision(5) << output_iter->pitch << ","
        << std::fixed << std::setprecision(5) << output_iter->yaw << ","
        << std::fixed << std::setprecision(5) << output_iter->east_vel << ","
        << std::fixed << std::setprecision(5) << output_iter->north_vel << ","
        << std::fixed << std::setprecision(5) << output_iter->up_vel
        << std::endl;
  }
  // clang-format on
  ofs.close();
}

void PoslvTrajectoryHandler::addTrajectoryPoint(PoslvTrajectoryPoint trajectory_point)
{
  traj_.push_back(trajectory_point);
}

PoslvTrajectoryPoint PoslvTrajectoryHandler::getClosestOutput(double in_time)
{
  if (traj_.size() == 0)
  {
    std::cerr << "\033[1;31mError: The input trajectory has not been set! \033[0m" << std::endl;
    exit(4);
  }

  // In case the exact match cannot be found
  // These variables are for getting the closest possible pose
  int best_match_id = 0;
  double min_t_diff = -1;
  for (int i = 0; i < traj_.size(); i++)
  {
    double t_diff = fabs(in_time - traj_[i].header.stamp.toSec());
    if ((t_diff < min_t_diff) || (min_t_diff < 0))
    {
      min_t_diff = t_diff;
      best_match_id = i;
    }
    if (t_diff < 0.01)
    {
      return traj_[i];
    }
  }

  return traj_[best_match_id];
}

ros::Time PoslvTrajectoryHandler::stringToROSTime(std::string& input)
{
  struct tm mid_time;
  strptime(input.c_str(), "%m/%d/%Y %H:%M:%S", &mid_time);
  time_t time = mktime(&mid_time);

  char dnull[32];
  double a, b;
  double seconds;
  sscanf(input.c_str(), "%s %lf:%lf:%lf", dnull, &a, &b, &seconds);

  double rem = seconds - std::floor(seconds);

  double dtime = time + rem;

  ros::Time ros_time(dtime);

  return ros_time;
}

std::vector<std::string> PoslvTrajectoryHandler::split(std::string& input, char delimiter)
{
  std::istringstream stream(input);
  std::string field;
  std::vector<std::string> result;

  while (std::getline(stream, field, delimiter))
  {
    result.push_back(field);
  }
  return result;
}
