#include "trajectory_handler/trajectory_handler.h"

TrajectoryHandler::TrajectoryHandler()
{
  is_twist_filled_ = false;
  l2b_matrix_ = Eigen::Matrix4f::Identity();
  print_point_ = 0;
}

void TrajectoryHandler::setTraj(std::vector<TrajectoryPoint> in_traj)
{
  traj = in_traj;
  // Check if the input trajectory contains localizer and base_link
  // Trajectory made by fix2tf or nmea2tf only contain tf::Transform
  for (int i = 0; i < traj.size(); i++)
  {
    Pose empty_pose;
    if (traj[i].localizer == empty_pose)
    {
      traj[i].localizer = pose_conversions::TF2Pose(traj[i].transform);
      traj[i].base_link = pose_conversions::matrix2Pose(pose_conversions::pose2Matrix(traj[i].localizer) * l2b_matrix_);
    }
  }

  print_point_ = 0;
}

void TrajectoryHandler::readCSV(std::string log_name)
{
  // Load the output pose file
  std::ifstream ifs(log_name);
  if (!ifs)
  {
    std::cerr << "\033[1;31mError: Output pose file not found!! \033[0m" << '\n';
    exit(2);
  }

  // Parse the contents
  std::vector<TrajectoryPoint> input_traj;
  int count = 0;
  std::string line;
  while (getline(ifs, line))
  {
    // Skip the header
    if (count > 0)
    {
      TrajectoryPoint item;
      // output_pose item;
      std::vector<std::string> str_vec = split(line, ',');
      // Parse header information
      item.header.seq = std::stoi(str_vec.at(0));
      item.header.stamp = str2ROSTime(str_vec.at(1));
      item.header.frame_id = str_vec.at(2);
      // Parse localizer position
      tf::Vector3 v(std::stof(str_vec.at(5)), std::stof(str_vec.at(6)), std::stof(str_vec.at(7)));
      item.transform.setOrigin(v);
      // Parse localizer orientaiton
      tf::Quaternion q;
      q.setRPY(std::stof(str_vec.at(8)), std::stof(str_vec.at(9)), std::stof(str_vec.at(10)));
      item.transform.setRotation(q);
      // Parse localizer Pose
      item.localizer.x = std::stof(str_vec.at(5));
      item.localizer.y = std::stof(str_vec.at(6));
      item.localizer.z = std::stof(str_vec.at(7));
      item.localizer.roll = std::stof(str_vec.at(8));
      item.localizer.pitch = std::stof(str_vec.at(9));
      item.localizer.yaw = std::stof(str_vec.at(10));
      // Parse base_link Pose
      item.base_link.x = std::stof(str_vec.at(11));
      item.base_link.y = std::stof(str_vec.at(12));
      item.base_link.z = std::stof(str_vec.at(13));
      item.base_link.roll = std::stof(str_vec.at(14));
      item.base_link.pitch = std::stof(str_vec.at(15));
      item.base_link.yaw = std::stof(str_vec.at(16));
      // Parse matching quality
      item.fitness_score = std::stof(str_vec.at(17));
      item.transformation_probability = std::stof(str_vec.at(18));
      // Parse reliability of a TrajectoryPoint
      // If clause to support old CSV files
      if (str_vec.size() > 19)
      {
        item.reliability = std::stoi(str_vec.at(19));
      }
      // Store the output in a vector
      input_traj.push_back(item);
    }
    count++;
  }
  // Set the trajectory data
  setTraj(input_traj);
}

void TrajectoryHandler::writeCSV(std::string file_name)
{
  if (traj.size() == 0)
  {
    std::cerr << "\033[1;31mError: The input trajectory has not been set! \033[0m" << std::endl;
    exit(4);
  }
  else
  {
    // Set log file name.
    std::ofstream ofs;
    ofs.open(file_name.c_str(), std::ios::app);
    if (!ofs.is_open())
    {
      std::cerr << "\033[1;31mError: Could not open " << file_name << ". \033[0m" << std::endl;
      exit(2);
    }

    if (print_point_ == 0)
    {
      // Write CSV file header
      // clang-format off
      ofs   << "input->header.seq"          << ","
            << "input->header.stamp"        << ","
            << "input->header.frame_id"     << ","
            << "scan_ptr->size()"           << ","
            << "filtered_scan_ptr->size()"  << ","
            << "localizer_pose.x"           << ","
            << "localizer_pose.y"           << ","
            << "localizer_pose.z"           << ","
            << "localizer_pose.roll"        << ","
            << "localizer_pose.pitch"       << ","
            << "localizer_pose.yaw"         << ","
            << "current_pose.x"             << ","
            << "current_pose.y"             << ","
            << "current_pose.z"             << ","
            << "current_pose.roll"          << ","
            << "current_pose.pitch"         << ","
            << "current_pose.yaw"           << ","
            << "fitness_score"              << ","
            << "transformation_probability" << ","
            << "reliability"
            << std::endl;
    }

    for (auto output_iter = traj.begin() + print_point_; output_iter != traj.end(); ++output_iter)
    {
      ofs << output_iter->header.seq
          << "," << output_iter->header.stamp
          << "," << output_iter->header.frame_id
          << "," << 0 // scan_ptr->size()
          << "," << 0 // filtered_scan_ptr->size()
          << "," << std::fixed << std::setprecision(5) << output_iter->localizer.x
          << "," << std::fixed << std::setprecision(5) << output_iter->localizer.y
          << "," << std::fixed << std::setprecision(5) << output_iter->localizer.z
          << "," << output_iter->localizer.roll
          << "," << output_iter->localizer.pitch
          << "," << output_iter->localizer.yaw
          << "," << std::fixed << std::setprecision(5) << output_iter->base_link.x
          << "," << std::fixed << std::setprecision(5) << output_iter->base_link.y
          << "," << std::fixed << std::setprecision(5) << output_iter->base_link.z
          << "," << output_iter->base_link.roll
          << "," << output_iter->base_link.pitch
          << "," << output_iter->base_link.yaw
          << "," << output_iter->fitness_score
          << "," << output_iter->transformation_probability
          << "," << output_iter->reliability
          << std::endl;
    }
    // clang-format on
    ofs.close();
  }

  print_point_ = traj.size();
}

void TrajectoryHandler::addTrajectoryPoint(TrajectoryPoint trajectory_point)
{
  if (traj.size() == 0)
  {
    // Set a temporary log file name.
    temp_log_file.open("/tmp/temp_trajectory.csv", std::ios::app);
    // Write CSV file header
    // clang-format off
    temp_log_file << "input->header.seq"          << ","
                  << "input->header.stamp"        << ","
                  << "input->header.frame_id"     << ","
                  << "scan_ptr->size()"           << ","
                  << "filtered_scan_ptr->size()"  << ","
                  << "localizer_pose.x"           << ","
                  << "localizer_pose.y"           << ","
                  << "localizer_pose.z"           << ","
                  << "localizer_pose.roll"        << ","
                  << "localizer_pose.pitch"       << ","
                  << "localizer_pose.yaw"         << ","
                  << "current_pose.x"             << ","
                  << "current_pose.y"             << ","
                  << "current_pose.z"             << ","
                  << "current_pose.roll"          << ","
                  << "current_pose.pitch"         << ","
                  << "current_pose.yaw"           << ","
                  << "fitness_score"              << ","
                  << "transformation_probability" << ","
                  << "reliability"
                  << std::endl;
  }
  else
  {
    temp_log_file << trajectory_point.header.seq
                  << "," << trajectory_point.header.stamp
                  << "," << trajectory_point.header.frame_id
                  << "," << 0 // scan_ptr->size()
                  << "," << 0 // filtered_scan_ptr->size()
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.localizer.x
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.localizer.y
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.localizer.z
                  << "," << trajectory_point.localizer.roll
                  << "," << trajectory_point.localizer.pitch
                  << "," << trajectory_point.localizer.yaw
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.base_link.x
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.base_link.y
                  << "," << std::fixed << std::setprecision(5) << trajectory_point.base_link.z
                  << "," << trajectory_point.base_link.roll
                  << "," << trajectory_point.base_link.pitch
                  << "," << trajectory_point.base_link.yaw
                  << "," << trajectory_point.fitness_score
                  << "," << trajectory_point.transformation_probability
                  << "," << trajectory_point.reliability
                  << std::endl;
    // clang-format on
  }
  traj.push_back(trajectory_point);
}

TrajectoryPoint TrajectoryHandler::getClosestOutput(double in_time)
{
  if (traj.size() == 0)
  {
    std::cerr << "\033[1;31mError: The input trajectory has not been set! \033[0m" << std::endl;
    exit(4);
  }
  else
  {
    int start_index = 0;
    // In case the exact match cannot be found
    // These variables are for getting the closest possible pose
    int best_match_id = start_index;
    double min_t_diff = -1;
    for (int i = start_index; i < traj.size(); ++i)
    {
      double t_diff = fabs(in_time - traj[i].header.stamp.toSec());
      if ((t_diff < min_t_diff) || (min_t_diff < 0))
      {
        min_t_diff = t_diff;
        best_match_id = i;
      }
      if (t_diff < 0.01)
      {
        return traj[i];
      }
    }
    return traj[best_match_id];
  }
}

void TrajectoryHandler::fillTwist()
{
  // Filling Twist using interpolation
  // Next pose - Previous pose / Next stamp - Previous stamp
  for (int i = 0; i < traj.size(); i++)
  {
    // Skip first and last point
    if ((i > 1) && (i < traj.size() - 1))
    {
      // Calcualte different between the next pose and the previous pose
      Pose diff_pose = traj[i + 1].localizer - traj[i - 1].localizer;
      // Wrapping roll
      if (diff_pose.roll > M_PI)
      {
        diff_pose.roll -= 2 * M_PI;
      }
      else if (diff_pose.roll < -M_PI)
      {
        diff_pose.roll += 2 * M_PI;
      }
      // Wrapping pitch
      if (diff_pose.pitch > M_PI)
      {
        diff_pose.pitch -= 2 * M_PI;
      }
      else if (diff_pose.pitch < -M_PI)
      {
        diff_pose.pitch += 2 * M_PI;
      }
      // Wrapping yaw
      if (diff_pose.yaw > M_PI)
      {
        diff_pose.yaw -= 2 * M_PI;
      }
      else if (diff_pose.yaw < -M_PI)
      {
        diff_pose.yaw += 2 * M_PI;
      }
      // Calculate diff rate
      double diff_stamp = traj[i + 1].header.stamp.toSec() - traj[i - 1].header.stamp.toSec();
      Pose diff_rate = (diff_pose / diff_stamp);
      // Assing twist values
      traj[i].twist.linear.x = diff_rate.x;
      traj[i].twist.linear.y = diff_rate.y;
      traj[i].twist.linear.z = diff_rate.z;
      traj[i].twist.angular.x = diff_rate.roll;
      traj[i].twist.angular.y = diff_rate.pitch;
      traj[i].twist.angular.z = diff_rate.yaw;
    }
    else
    {
      traj[i].twist.linear.x = 0.0;
      traj[i].twist.linear.y = 0.0;
      traj[i].twist.linear.z = 0.0;
      traj[i].twist.angular.x = 0.0;
      traj[i].twist.angular.y = 0.0;
      traj[i].twist.angular.z = 0.0;
    }
  }
  is_twist_filled_ = true;
}

std::vector<TrajectoryPoint> TrajectoryHandler::reverseTrajectory(std::vector<TrajectoryPoint> raw_output,
                                                                  const std::string& ndt_params)
{
  // Get the tranformation between base_link and localizer
  Eigen::Matrix4f tf_btol = pose_conversions::getB2LMatrix(ndt_params);
  // Get the transform between first and last poses of the trajectory
  Eigen::Matrix4f origin_transform_base_link = pose_conversions::pose2Matrix(raw_output.back().base_link).inverse();

  std::vector<TrajectoryPoint> reversed_output;
  for (auto output_iter = raw_output.begin(); output_iter != raw_output.end(); ++output_iter)
  {
    TrajectoryPoint current_mapping_output = *output_iter;
    Pose new_localizer = pose_conversions::matrix2Pose(
        origin_transform_base_link * pose_conversions::pose2Matrix(current_mapping_output.base_link) * tf_btol);
    Pose new_base_link = pose_conversions::matrix2Pose(origin_transform_base_link *
                                                       pose_conversions::pose2Matrix(current_mapping_output.base_link));
    current_mapping_output.localizer = new_localizer;
    current_mapping_output.base_link = new_base_link;
    reversed_output.insert(reversed_output.begin(), current_mapping_output);
  }

  return reversed_output;
}

Pose TrajectoryHandler::projectPose(Pose current_pose, double current_stamp, double estimated_last_point_stamp)
{
  if (!is_twist_filled_)
  {
    fillTwist();
  }
  // Deal with the initial value
  if (current_stamp > 0)
  {
    if (estimated_last_point_stamp > current_stamp)
    {
      geometry_msgs::Twist current_wist = getClosestOutput(current_stamp).twist;
      double time_diff = estimated_last_point_stamp - current_stamp;
      Pose projected_pose;
      projected_pose.x = current_pose.x + time_diff * current_wist.linear.x;
      projected_pose.y = current_pose.y + time_diff * current_wist.linear.y;
      projected_pose.z = current_pose.z + time_diff * current_wist.linear.z;
      projected_pose.roll = current_pose.roll + time_diff * current_wist.angular.x;
      projected_pose.pitch = current_pose.pitch + time_diff * current_wist.angular.y;
      projected_pose.yaw = current_pose.yaw + time_diff * current_wist.angular.z;
      return projected_pose;
    }
    else
    {
      std::cout << "Input timestamp is invalid!" << '\n';
    }
  }
  else
  {
    return current_pose;
  }
}

ros::Time TrajectoryHandler::str2ROSTime(std::string& input)
{
  ros::Time time;
  std::vector<std::string> str_vec = split(input, '.');
  time.sec = std::stoi(str_vec.at(0));
  time.nsec = std::stoi(str_vec.at(1));
  return time;
}

std::vector<std::string> TrajectoryHandler::split(std::string& input, char delimiter)
{
  std::istringstream stream(input);
  std::string field;
  std::vector<std::string> result;
  while (std::getline(stream, field, delimiter))
  {
    result.push_back(field);
  }
  return result;
}

void TrajectoryHandler::setTFMatrix(std::string config_file)
{
  l2b_matrix_ = pose_conversions::getB2LMatrix(config_file);
}
